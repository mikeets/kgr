//
// Created by mirza.hrvanovic on 9/27/2022.
//

#ifndef KGR_KGRFORMULE_H
#define KGR_KGRFORMULE_H

#include <string>
#include <math.h>
#include <stdexcept>
#include <iostream>
#include "../models/Koordinata.h"

using namespace std;

class KGRFormule {
public:
    static Koordinata * TrodimenzionalnaOsnaSimetrija(string ravan, Koordinata *tackaP, Koordinata *tackaA) {
        double x;
        double y;
        double z;

        if(ravan == "yz") {
            x = (-1 * tackaA->x) + (2 * tackaP->x);
            y = tackaA->y;
            z = tackaA->z;
        } else if(ravan == "xz") {
            x = tackaA->x;
            y = (-1 * tackaA->y) + (2 * tackaP->y);
            z = tackaA->z;
        } else if(ravan == "xy") {
            x = tackaA->x;
            y = tackaA->y;
            z = (-1 * tackaA->z) + (2 * tackaP->z);
        } else {
            throw invalid_argument("GRESKA: Molimo da proslijedite yz,xz ili xy");
        }

        return new Koordinata(x,y,z);
    }

    static Koordinata * TrodimenzionalaRotacijaOkoTacke(string paralelnaOsi, Koordinata* tackaP, Koordinata *tackaA, double ugao) {
        double x;
        double y;
        double z;

        if(paralelnaOsi == "z") {
            x = cos(ugao) * tackaA->x;
            x += -(sin(ugao)) * tackaA->y;
            x += (tackaP->x * (1 - cos(ugao))) + (tackaP->y * (sin(ugao)));

            y = sin(ugao) * tackaA->x;
            y += cos(ugao) * tackaA->y;
            y += (tackaP->y * (1 - cos(ugao))) - (tackaP->x * (sin(ugao)));

            z += tackaA->z;
        } else if(paralelnaOsi == "x") {
            x = tackaA->x;

            y = cos(ugao) * tackaA->y;
            y += -(sin(ugao)) * tackaA->z;
            y += (tackaP->y * (1 - cos(ugao))) + (tackaP->z * (sin(ugao)));

            z = sin(ugao) * tackaA->y;
            z += cos(ugao) * tackaA->z;
            z += (tackaP->z * (1 - cos(ugao))) - (tackaP->y * (sin(ugao)));
        } else if(paralelnaOsi == "y") {
            x = cos(ugao) * tackaA->x;
            x += sin(ugao) * tackaA->z;
            x += (tackaP->x * (1 - cos(ugao))) - (tackaP->z * (sin(ugao)));

            y = tackaA->y;

            z = -(sin(ugao)) * tackaA->x;
            z += cos(ugao) * tackaA->z;
            z += (tackaP->z * (1 - cos(ugao))) + (tackaP->x * (sin(ugao)));
        } else {
            throw invalid_argument("GRESKA: Molimo da proslijedite x, y ili z");
        }

        return new Koordinata(x,y,z);
    }
};


#endif //KGR_KGRKALKULATOR_H
