//
// Created by mirza.hrvanovic on 9/29/2022.
//

#ifndef KGR_KORISNICKIINTERFEJS_H
#define KGR_KORISNICKIINTERFEJS_H

#include <iostream>
#include "../models/Koordinata.h"
#include "../util/KGRFormule.h"

using namespace std;

class KorisnickiInterfejs {
public:
    void pokreni() {
        Koordinata *A;
        Koordinata *P;

        cout << "KGR\n";

        int step = 0;
        double Ax, Ay, Az;

        do {
            cout << "Unesite x za tacku A:\t";

            if(!(cin >> Ax)) {
                cout << "GRESKA: Ax nije validan! Molimo da unesete broj!\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
            }

            cout << "Unesite y za tacku A:\t";
            if (!(cin >> Ay)) {
                cout << "GRESKA: Ay nije validan! Molimo da unesete broj!\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
            }

            cout << "Unesite z za tacku A:\t";
            if (!(cin >> Az)) {
                cout << "GRESKA: Az nije validan! Molimo da unesete broj!\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
            }

            step++;
        } while(step == 0);


        double Px, Py, Pz;
        do {
            cout << "\nUnesite x za tacku P:\t";
            if (!(cin >> Px)) {
                cout << "GRESKA: Px nije validan! Molimo da unesete broj!\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
            }

            cout << "Unesite y za tacku P:\t";
            if (!(cin >> Py)) {
                cout << "GRESKA: Py nije validan! Molimo da unesete broj!\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
            }

            cout << "Unesite z za tacku P:\t";
            if (!(cin >> Pz)) {
                cout << "GRESKA: Pz nije validan! Molimo da unesete broj!\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
            }

            step++;
        } while(step == 1);


        double ugao;
        do {
            cout << "\nTacka P za ugao:\t";

            if (!(cin >> ugao)) {
                cout << "GRESKA: Molimo da unesete validan ugao!\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
            }

            if (ugao > 360 || ugao < -360) {
                cout << "GRESKA: Molimo da unesete validan ugao!\n";
                ugao = NULL;
                continue;
            }

            step++;
        } while (step == 2);

        string ravan;
        string prava;

        do {
            cout << "S obzirom na ravan yz/xz/xy:\t";

            cin >> ravan;

            if (!(ravan == "yz" || ravan == "xz" || ravan == "xy")) {
                cout << "GRESKA: Molimo da unesete validu vrijednost, yz,xz ili xy!\n";
                continue;
            }

            step++;
        } while (step == 3);

        do {
            cout << "S obzirom na pravu x/y/z:\t";

            cin >> prava;
            if (!(prava == "x" || prava == "y" || prava == "z")) {
                cout << "GRESKA: Molimo da unesete validu vrijednost, x,y ili z!\n";
                continue;
            }

            step++;
        } while (step == 4);

        A = new Koordinata(Ax, Ay, Az);
        P = new Koordinata(Px, Py, Pz);


        cout << "Tacka A(" << A->x << "," << A->y << "," << A->z << ")\n";
        cout << "Tacka P(" << P->x << "," << P->y << "," << P->z << ")\n";

        cout << "\nRezultat:\n";

        Koordinata* APrim = KGRFormule::TrodimenzionalnaOsnaSimetrija(ravan, P, A);

        cout << "\nA'(" << APrim->x << "," << APrim->y << "," << APrim->z << ")";

        Koordinata *ASekund = KGRFormule::TrodimenzionalaRotacijaOkoTacke(prava, P, APrim, ugao);

        cout << "\nA''(" << ASekund->x << "," << ASekund->y << "," << ASekund->z << ")\n";
    }
};


#endif //KGR_KORISNICKIINTERFEJS_H
