//
// Created by mirza.hrvanovic on 9/29/2022.
//

#ifndef KGR_KOORDINATA_H
#define KGR_KOORDINATA_H


class Koordinata {
public:
    double x;
    double y;
    double z;

    Koordinata(double x, double y, double z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }
};


#endif //KGR_3D_TRANSFORMACIJA_KOORDINATA_H
